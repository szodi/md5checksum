package md5checksum;

import java.io.FileInputStream;
import java.io.InputStream;
import java.security.MessageDigest;

public class MD5Checksum
{
	public static byte[] createChecksum(InputStream inputStream) throws Exception
	{
		byte[] buffer = new byte[8192];
		MessageDigest complete = MessageDigest.getInstance("MD5");
		int numRead;

		do
		{
			numRead = inputStream.read(buffer);
			if (numRead > 0)
			{
				complete.update(buffer, 0, numRead);
			}
		}
		while (numRead != -1);
		return complete.digest();
	}

	// see this How-to for a faster way to convert
	// a byte array to a HEX string
	public static String getMD5Checksum(String filename) throws Exception
	{
		InputStream fis = new FileInputStream(filename);
		String md5Checksum = getMD5Checksum(fis);
		fis.close();
		return md5Checksum;
	}

	public static String getMD5Checksum(InputStream inputStream) throws Exception
	{
		byte[] b = createChecksum(inputStream);
		String result = "";

		for (int i = 0; i < b.length; i++)
		{
			result += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
		}
		return result;
	}
}
